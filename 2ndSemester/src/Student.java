
//TODO Write class Javadoc

/**
 * Student class that inherit from Person class that contain both name and ID
 * @author Thanawit Gerdprasert
 * @version 2015.01.13
 */
public class Student extends Person {
	private long id;

	//	TODO Write constructor Javadoc
	/**
	 * Create Student Object and initialize the data from recieved parameter
	 * @param name name of the Object Student
	 * @param id identification number of Object Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object obj) {
		// (1) verify that obj is not null
		if (obj == null) return false;
		// (2) test if obj is the same class as "this" object
		if ( obj.getClass() != this.getClass() )
	 		return false;
		// (3) cast obj to this class's type
		Student other = (Student) obj;
		// (4) compare whatever values determine "equals"
		if ( this.id==other.id) 
			return true;
		return false; 

		

	}

}
